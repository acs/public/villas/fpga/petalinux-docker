FROM ubuntu:18.04

MAINTAINER z4yx <z4yx@users.noreply.github.com>

# Build with:
# docker build \
#   --build-arg PETA_VERSION=2018.1 \
#   --build-arg PETA_RUN_FILE=petalinux-v2018.1-final-installer.run \
#   -t petalinux:2018.1 .

ENV DEBIAN_FRONTEND=noninteractive

#install dependences:
RUN dpkg --add-architecture i386 && \
    apt-get update && \
    apt-get install -y -q gcc git make net-tools libncurses5-dev \
    tftpd zlib1g-dev libssl-dev flex bison libselinux1 gnupg wget \
    diffstat chrpath socat xterm autoconf libtool tar unzip texinfo \
    zlib1g-dev gcc-multilib build-essential zlib1g:i386 screen pax \
    gzip \
    locales sudo texinfo gzip unzip cpio chrpath autoconf lsb-release \
    libtool-bin kmod rsync bc u-boot-tools update-inetd xvfb \
    libgtk2.0-0 libsdl1.2-dev libglib2.0-dev lib32z1-dev expect gawk \
    && apt-get clean \
    && rm -rf /var/lib/apt/lists/*

ARG PETA_VERSION
ARG PETA_RUN_FILE

RUN locale-gen en_US.UTF-8 && update-locale

# Create a Vivado user
RUN adduser --disabled-password --gecos '' vivado && \
    usermod -aG sudo vivado && \
    echo "vivado ALL=(ALL) NOPASSWD: ALL" >> /etc/sudoers

RUN mkdir /installers
COPY accept-eula.sh /
COPY ${PETA_RUN_FILE} /

# Run the installer
RUN chmod a+rx /${PETA_RUN_FILE} && \
    chmod a+rx /accept-eula.sh && \
    mkdir -p /opt/Xilinx && \
    chmod 777 /tmp /opt/Xilinx && \
    cd /tmp && \
    sudo -u vivado -i /accept-eula.sh /${PETA_RUN_FILE} /opt/Xilinx/petalinux && \
    rm -f /${PETA_RUN_FILE} /accept-eula.sh

# Make /bin/sh symlink to bash instead of dash:
RUN echo "dash dash/sh boolean false" | debconf-set-selections
RUN dpkg-reconfigure dash

USER vivado
ENV HOME /home/vivado
ENV LANG en_US.UTF-8
RUN mkdir /home/vivado/project
WORKDIR /home/vivado/project

# Add vivado tools to path
RUN echo "source /opt/Xilinx/petalinux/settings.sh" >> /home/vivado/.bashrc
